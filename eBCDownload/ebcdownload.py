#!/usr/bin/python3

#########################################################################################
# ebcdownload.py                                                                        #
# >> Downloads Bosch eBike Connect rides                                                #
#  @CyclisteUrbain                                                                      #
#                                                                                       #
# v1.0 - 07/07/2022 - Initial version by @CyclisteUrbain                                #
# v1.1 - 09/07/2022 - Ride list is downloaded rather than extracted from file           #
# v1.2 - 16/07/2022 - Login/Password authentication (by @eMerzh)                                                                                     #
#                                                             <!            @           #
#                                                              !___   _____`\<,!_!      #
#                                                              !(*)!--(*)---/(*)        #
#########################################################################################

##############################################
# IMPORTS
import argparse
import json
import time
from datetime import datetime
from os import path

import requests

##############################################
# SETTINGS

# Default output location
set_output = "~/Documents/BoschEBCRides"
# Default maximum number of trips to download
set_maxtripts = 2000

# Colors
class fgc:
    HEADER = "\033[95m"
    OKBLUE = "\033[94m"
    OKGREEN = "\033[92m"
    WARNING = "\033[93m"
    FAIL = "\033[91m"
    ENDC = "\033[0m"
    BOLD = "\033[1m"


def utc2local(utc):
    epoch = time.mktime(utc.timetuple())
    offset = datetime.fromtimestamp(epoch) - datetime.utcfromtimestamp(epoch)
    return utc + offset


ridesdl_count = 0
ridesid_list = []

##############################################
# EXECUTION HEADER

print(
    "\r\n"
    + fgc.HEADER
    + "#**************************************************************************************#"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "# ebcdownload.py                                                                       #"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "# Downloads ride data from Bosch eBike Connect                                         #"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "#  v1.1 - 08/07/2022                                                                   #"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "#                                                             <!            @          #"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "#                                                              !___   _____`\<,!_!     #"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "# by @CyclisteUrbain                                           !(*)!--(*)---/(*)       #"
    + fgc.ENDC
)
print(
    fgc.HEADER
    + "########################################################################################"
    + fgc.ENDC
    + "\r\n"
)

##############################################
# ARGUMENTS CHECK

parser = argparse.ArgumentParser()
parser.add_argument(
    "--cookie",
    "-c",
    help="authentication cookie data (cookie name REMEMBER)",
    type=str,
    default="",
)
parser.add_argument(
    "--output", "-o", help="output directory", type=str, default=set_output
)
parser.add_argument(
    "--startdate",
    "-s",
    help="start date of the time range of rides to be dowloaded, formated as dd/mm/yyyy",
    type=str,
    default="",
)
parser.add_argument(
    "--enddate",
    "-e",
    help="end date of the time range of rides to be downloaded,  formated as dd/mm/yyyy (today by default)",
    type=str,
    default="",
)
parser.add_argument(
    "--maxtrips",
    "-m",
    help="maximum trips to be retrieved",
    type=int,
    default=set_maxtripts,
)
parser.add_argument(
    "--login", "-l", help="Login for the bosh connect Portal", type=str, default=""
)
parser.add_argument(
    "--password",
    "-p",
    help="Password for the bosh connect Portal",
    type=str,
    default="",
)

args = parser.parse_args()

#Requests for password if only a login was provided
password = args.password
if args.login and password == "":
    password = getpass.getpass('Please enter password for username '+args.login+':')

if args.login and args.password:
    login_url = "https://www.ebike-connect.com/ebikeconnect/api/portal/login/public"
    response = requests.post(
        login_url,
        json={"username": args.login, "password": args.password, "rememberme": True},
    )
    if error := response.json().get("errors"):
        print(fgc.FAIL + f"Error : Login error {error[0].get('message')}" + fgc.ENDC)
        quit()
    cookiedata = response.cookies.get("REMEMBER")
else:
    cookiedata = args.cookie

# Cookie
if cookiedata == "":
    print(
        fgc.FAIL
        + "Error : cookie data is empty, get it from your browser's devtools"
        + fgc.ENDC
    )
    print(parser.format_help())
    quit()

# Output is a directory
outpath = args.output
if not path.isdir(outpath):
    print(
        fgc.FAIL
        + "Error : output path "
        + outpath
        + " does not exist or is not a directory"
        + fgc.ENDC
    )
    print(parser.format_help())
    quit()

# Start Time is a date or is empty
startdate = args.startdate
if startdate == "":
    starttime = int(round((datetime.now().timestamp()) * 1000))
else:
    try:
        starttime_obj = datetime.strptime(startdate, "%d/%m/%Y")
        starttime = int(round((starttime_obj.timestamp()) * 1000))
    except ValueError as err:
        print(
            fgc.FAIL
            + "Error : start date "
            + startdate
            + " is not a valid date"
            + format(err)
            + fgc.ENDC
        )
        print(parser.format_help())
        quit()

# End Date is a date or is empty
enddate = args.enddate
if enddate == "":
    today = datetime.today()
    today_str = today.strftime("%Y-%m-%d")
    today_str = today_str + "-23:59:59.999999"
    endtime_obj = datetime.strptime(today_str, "%Y-%m-%d-%H:%M:%S.%f")
    endtime = int(round((endtime_obj.timestamp()) * 1000))
else:
    endtime_str = enddate + "-23:59:59.999999"
    try:
        endtime_obj = datetime.strptime(endtime_str, "%Y-%m-%d-%H:%M:%S.%f")
        endtime = int(round((endtime_obj.timestamp()) * 1000))
    except ValueError as err:
        print(
            fgc.FAIL
            + "Error : end date "
            + endtime_str
            + " is not a valid date : "
            + format(err)
            + fgc.ENDC
        )
        print(parser.format_help())
        quit()

# Max Trips
maxtrips = args.maxtrips

##############################################
# RIDES LIST DOWNLOAD

dts = datetime.fromtimestamp(starttime / 1000)
dte = datetime.fromtimestamp(endtime / 1000)

print(
    fgc.BOLD
    + "Downloading ride list from "
    + str(maxtrips)
    + " trip(s) from "
    + dts.strftime("%d/%m/%Y %H:%M")
    + " to "
    + dte.strftime("%d/%m/%Y %H:%M")
    + "."
    + fgc.ENDC
)

URL = (
    "https://www.ebike-connect.com/ebikeconnect/api/portal/activities/trip/headers?max="
    + str(maxtrips)
    + "&offset="
    + str(endtime)
)
RefURL = "https://www.ebike-connect.com/activities/"
headers = {
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:102.0) Gecko/20100101 Firefox/102.0",
    "Accept": "application/vnd.ebike-connect.com.v4+json, application/json",
    "Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
    "Accept-Encoding": "gzip, deflate, br",
    "Protect-from": "CSRF",
    "Cache-Control": "no-cache",
    "Pragma": "no-cache",
    "If-Modified-Since": "Mon, 26 Jul 1997 05:00:00 GMT",
    "DNT": "1",
    "Connection": "keep-alive",
    "Referer": RefURL,
    "Cookie": "REMEMBER=" + cookiedata,
    "Sec-Fetch-Dest": "empty",
    "Sec-Fetch-Mode": "cors",
    "Sec-Fetch-Site": "same-origin",
}
print("   Fetching URL " + URL)

response = requests.get(url=URL, headers=headers)
if response.status_code == 200:
    print(fgc.OKGREEN + "   OK" + fgc.ENDC + " : content retrieved")
else:
    print(fgc.FAIL + "   Could not fetch trips data." + fgc.ENDC)
    print(
        fgc.FAIL
        + "   Error : "
        + str(response.status_code)
        + ", "
        + response.reason
        + fgc.ENDC
    )
    quit()

##############################################
# EXTRACT RIDES ID FROM PREVIOUS RESPONSE
print(fgc.BOLD + "Extracting rides ID from response" + fgc.ENDC)

for trip in json.loads(response.content):
    for ride in trip["ride_headers"]:
        if int(ride["start_time"]) > starttime:
            print(
                "      Ride "
                + ride["id"]
                + ": "
                + ride["title"]
                + ", started on "
                + datetime.fromtimestamp(int(ride["start_time"]) / 1000).strftime(
                    "%d/%m/%Y %H:%M"
                )
            )
            ridesid_list.append(ride["id"])

# Removing duplicates
ridesid_list = list(dict.fromkeys(ridesid_list))
print(fgc.BOLD + str(len(ridesid_list)) + " ride(s) in file" + fgc.ENDC)


##############################################
# RIDE DATA DOWNLOAD

print(fgc.BOLD + "Dowloading rides data to folder " + outpath + fgc.ENDC)

GET_BaseURL = "https://www.ebike-connect.com/ebikeconnect/api/activities/ride/details/"
GET_RefBaseURL = "https://www.ebike-connect.com/activities/details/ride/"
ridesdl_ok = 0
ridesdl_fail = 0

for ride_id in ridesid_list:
    URL = GET_BaseURL + str(ride_id)
    RefURL = GET_RefBaseURL + str(ride_id)
    headers = {
        "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:102.0) Gecko/20100101 Firefox/102.0",
        "Accept": "application/vnd.ebike-connect.com.v4+json, application/json",
        "Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
        "Accept-Encoding": "gzip, deflate, br",
        "Protect-from": "CSRF",
        "Cache-Control": "no-cache",
        "Pragma": "no-cache",
        "If-Modified-Since": "Mon, 26 Jul 1997 05:00:00 GMT",
        "DNT": "1",
        "Connection": "keep-alive",
        "Referer": RefURL,
        "Cookie": "REMEMBER=" + cookiedata,
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-origin",
    }
    ridesdl_count += 1
    print(
        fgc.BOLD
        + "Downloading ride data "
        + str(ridesdl_count)
        + "/"
        + str(len(ridesid_list))
        + fgc.ENDC
    )
    print("   URL : " + URL)

    response = requests.get(url=URL, headers=headers)
    if response.status_code == 200:
        outfile = outpath + "/BoschEBC-" + str(ride_id) + ".json"
        open(outfile, "wb").write(response.content)
        print(fgc.OKGREEN + "    OK" + fgc.ENDC + " : content wrote to " + outfile)
        ridesdl_ok += 1

    if response.status_code == 403:
        print("   Reason : " + str(response.reason))
        print(
            fgc.FAIL
            + "    Failure : Unauthorized, cookie data may not be right or may be outaded"
            + fgc.ENDC
        )
        print(
            fgc.FAIL
            + "    This error is fatal, not downloading the other files"
            + fgc.ENDC
        )
        ridesdl_fail += 1
        break

    if response.status_code == 404:
        print("   Reason : " + str(response.reason))
        print("   Error : " + str(response.text))
        print(
            fgc.FAIL + "    Failure : Ride ID" + str(ride_id) + " not found" + fgc.ENDC
        )
        ridesdl_fail += 1

    if response.status_code == 400:
        print("   Reason : " + str(response.reason))
        print(
            fgc.FAIL
            + "    Failure : Ride ID"
            + str(ride_id)
            + " doesn't refer to a ride (maybe a trip)"
            + fgc.ENDC
        )
        ridesdl_fail += 1

##############################################
# SUMMARY

print(fgc.BOLD + "******************************" + fgc.ENDC)
print(fgc.BOLD + "Script end" + fgc.ENDC)
print(fgc.BOLD + str(len(ridesid_list)) + " ride(s) in source file" + fgc.ENDC)
print(fgc.BOLD + str(ridesdl_ok) + " ride(s) downloaded successfully" + fgc.ENDC)
print(fgc.BOLD + str(ridesdl_fail) + " ride(s) not downloaded" + fgc.ENDC)
